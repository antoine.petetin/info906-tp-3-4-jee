<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>


<t:layout title="Create a backlog entry">
	<jsp:attribute name="body_area">

            <div class="card">
                <div class="card-body">

                    <h1>Create a backlog entry</h1>


                    <c:choose>
                        <c:when test="${error!=null}">
                             <div class="alert alert-danger" role="alert">
                                     ${error}
                             </div>
                            <c:remove var="error" scope="session"/>

                        </c:when>

                    </c:choose>


                    <form
                            <c:choose>
                                <c:when test="${entry!=null}">
                                     action="../backlogentry/update?id=${entry.id}"
                                </c:when>
                                <c:otherwise>
                                    action="../backlogentry/create"
                                </c:otherwise>

                            </c:choose>
                            method="post">
                        <div class="form-group">
                            <label for="name">Name</label>
                            <input type="text" name="name" class="form-control" id="name" placeholder="Enter name"
                            <c:choose>
                                <c:when test="${entry!=null}">
                                     value="${entry.name}"
                                </c:when>

                            </c:choose>>

                        </div>
                        <div class="form-group">
                            <label for="description">Description</label>
                            <input type="text" name="description" class="form-control" id="description" placeholder="Enter description"
                            <c:choose>
                                <c:when test="${entry!=null}">
                                       value="${entry.description}"
                                </c:when>

                            </c:choose>>

                        </div>
                        <div class="form-group">
                            <label for="estimation">Estimation</label>
                            <input type="text" name="estimation" class="form-control" id="estimation" placeholder="Enter estimation"
                            <c:choose>
                                <c:when test="${entry!=null}">
                                       value="${entry.estimation}"
                                </c:when>

                            </c:choose>>

                        </div>
                        <div class="form-group">
                            <label for="priority">Priority</label>
                            <input type="text" name="priority" class="form-control" id="priority" placeholder="Enter priority"
                            <c:choose>
                                <c:when test="${entry!=null}">
                                       value="${entry.priority}"
                                </c:when>
                            </c:choose>>

                        </div>


                        <c:choose>
                            <c:when test="${entry!=null}">
                                <button type="submit" class="btn btn-primary">Update</button>
                            </c:when>
                            <c:otherwise>
                                <button type="submit" class="btn btn-primary">Create</button>
                            </c:otherwise>

                        </c:choose>

                    </form>


                </div>
            </div>


        </jsp:attribute>

</t:layout>
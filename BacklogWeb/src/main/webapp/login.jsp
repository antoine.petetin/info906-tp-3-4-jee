<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>


<t:layout title="Login">
	<jsp:attribute name="body_area">

            <div class="card">
                <div class="card-body">

                    <h1>Please sign in </h1>


                    <c:choose>
                        <c:when test="${error!=null}">
                             <div class="alert alert-danger" role="alert">
                                     ${error}
                             </div>

                            <c:remove var="error" scope="session"/>


                        </c:when>

                    </c:choose>




                    <form action="./login" method="post">
                        <div class="form-group">
                            <label for="exampleInputEmail1">Email address</label>
                            <input type="email" name="email" class="form-control" id="exampleInputEmail1" placeholder="Enter email">

                        </div>
                        <div class="form-group">
                            <label for="exampleInputPassword1">Password</label>
                            <input type="password" name="password" class="form-control" id="exampleInputPassword1" placeholder="Password">
                        </div>

                        <button type="submit" class="btn btn-primary">Log in</button>
                    </form>


                </div>
            </div>


        </jsp:attribute>

</t:layout>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>


<t:layout title="Select agency">
	<jsp:attribute name="body_area">

            <div class="card">
                <div class="card-body">

                    <h1>Please choose one of your agency</h1>

                    <c:choose>
                        <c:when test="${error!=null}">
                             <div class="alert alert-danger" role="alert">
                                     ${error}

                             </div>
                               <c:remove var="error" scope="session"/>

                        </c:when>

                        <c:when test="${message!=null}">
                             <div class="alert alert-success" role="alert">
                                     ${message}

                             </div>
                            <c:remove var="message" scope="session"/>

                        </c:when>


                    </c:choose>



                    <form action="./agencyselection" method="post">
                        <div class="form-group">

                            <select name="agencyId" id="agencyId">
                                <option value="-1">Select agency:</option>
                                <c:forEach items="${agencies}"  var="agency">

                                    <option value="${agency.id}">${agency.name}</option>

                                </c:forEach>
                            </select>

                        </div>
                        <button type="submit" class="btn btn-primary">Select</button>
                    </form>


                </div>
            </div>


        </jsp:attribute>

</t:layout>
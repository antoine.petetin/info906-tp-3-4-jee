<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>


<t:layout title="Create a agency">
	<jsp:attribute name="body_area">

            <div class="card">
                <div class="card-body">

                    <h1>Create a agency</h1>


                    <c:choose>
                        <c:when test="${error!=null}">
                             <div class="alert alert-danger" role="alert">
                                     ${error}
                             </div>
                            <c:remove var="error" scope="session"/>

                        </c:when>

                    </c:choose>


                    <form action="../agency/create" method="post">
                        <div class="form-group">
                            <label for="name">Name</label>
                            <input type="text" name="name" class="form-control" id="name" placeholder="Enter name"

                        </div>

                        <button type="submit" class="btn btn-primary">Create</button>
                    </form>


                </div>
            </div>


        </jsp:attribute>

</t:layout>
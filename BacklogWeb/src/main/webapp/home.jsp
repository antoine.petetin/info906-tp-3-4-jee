<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>

<t:layout title="Welcome page">
	<jsp:attribute name="body_area">

		<h1>${agency.name}</h1>
		<div class="row">
			<div class="col-md-4">
				<a href="./backlogentry/create" class="btn btn-primary mb-2">Create a backlog entry</a>
			</div>

		</div>
		<div class="row">
				<c:forEach items="${agency.backlog}"  var="entry">
					<div class="col-md-4">
						<div class="card mb-4">
							<div class="card-header">
								<div class="col-md-12">

									<div class="row">
										<a href="/BacklogWeb/backlogentry/update?id=${entry.id}" class="btn btn-primary btn-sm mb-2 mr-2 float-right">Update</a>

										<form class="d-inline" action="/BacklogWeb/backlogentry/delete" method ="post">
											<input type="hidden" name="id" value="${entry.id}"/>
											<input type="submit" class="btn btn-danger btn-sm mb-2 float-right" value="Delete"/>
										</form>
									</div>

									<label class="float-none">
											${entry.name}
									</label>
									<label class="float-right">${entry.estimation}</label>

									<div class="card-body">
										<p class="card-text">${entry.description}</p>
										<p><a id="commentsToggleText${entry.id}" href="#" onclick="commentsToggle(${entry.id})">Voir les commentaires</a></p>
										<p class="blockquote-footer">Created the <fmt:formatDate type = "both" value = "${entry.creationDate}" /></p>


											<div>
												<textarea id="addCommentText${entry.id}" placeholder="Ajouter un commentaire"></textarea>
												<button class="btn btn-primary btn-sm" onclick="addComment(${entry.id})">Commenter</button>
											</div>


										<table id="commentaires${entry.id}" visible="false" class="table">
											<tbody>
											<c:forEach items="${entry.comments}" var="comment">
												<tr>
													<td>
														<div>
															<p><b>${comment.employee.fullName}</b></p>
															<p>${comment.content}</p>
															<p class="blockquote-footer">Created the <fmt:formatDate type = "both" value = "${comment.creationDate}" /></p>
														</div>
													</td>
												</tr>
											</c:forEach>
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
					</div>
				</c:forEach>
			<input type="hidden" id="employeeId" value="${sessionScope.employeeId}"/>
		</div>

		<script>
            function addComment(backlogEntryId) {
                var comment = $("#addCommentText" + backlogEntryId).val();
                var employeeId = $('#employeeId').val();
                $.post("./comment", {comment: comment, employeeId: employeeId, backlogId: backlogEntryId})
                    .done(function (comment) {
                        var tr = $("<tr/>");
                        var td = $("<td/>");
                        var div = $("<div/>");
                        div.append('<p><b>' + comment.employee + '</b></p>');
                        div.append('<p>' + comment.content + '</p>');
                        div.append('<p class="blockquote-footer">Created the '+ comment.creationDate + '</p>');
                        td.append(div);
                        tr.append(td);
                        $("#commentaires" + backlogEntryId +" tbody").append(tr);

                        if($('#commentaires' + backlogEntryId).attr('visible') == 'false')
	                        commentsToggle(backlogEntryId);


                    })
                    .fail(function (xhr, status, error) {
                        // error handling
                        alert("error : " + error);
                    });
            }

            function commentsToggle(backlogEntryId){
                $('#commentaires'+backlogEntryId).toggle('fast');
                if($('#commentaires'+backlogEntryId).attr('visible')=='true') {
                    $('#commentsToggleText' + backlogEntryId).html('Voir les commentaires');
                    $('#commentaires'+backlogEntryId).attr('visible','false');
                }else {
                    $('#commentsToggleText' + backlogEntryId).html('Cacher les commentaires');
                    $('#commentaires' + backlogEntryId).attr('visible', 'true');
                }
            }

            $('table').each(function(index,elem){
                $(elem).toggle();
            });

		</script>
	</jsp:attribute>
</t:layout>
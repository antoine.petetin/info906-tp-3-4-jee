package fr.usmb.m2isc.javaee.backlogs.web;

import fr.usmb.m2isc.javaee.backlogs.ejb.BacklogEntryRepositoryInterface;
import fr.usmb.m2isc.javaee.backlogs.jpa.BacklogEntry;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * Servlet utilisee pour modifier une entrée d'un backlog existant
 */
@WebServlet("/backlogentry/update")
public class UpdateBacklogEntryServlet extends HttpServlet{

	private static final long serialVersionUID = 5236668439173484090L;
	@EJB
	private BacklogEntryRepositoryInterface backlogEntryRepository;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public UpdateBacklogEntryServlet() {
		super();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		HttpSession session = request.getSession(true);
		if(session.getAttribute("employeeId") == null)
		{
			response.sendRedirect("../login");
			return;
		}else {
			int backlogEntryId = Integer.valueOf(request.getParameter("id"));

			BacklogEntry b = backlogEntryRepository.get(backlogEntryId);

			request.setAttribute("entry", b);

			request.getRequestDispatcher("../backlogEntryForm.jsp").forward(request, response);
		}

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		HttpSession session = request.getSession(true);

		int backlogEntryId = Integer.valueOf(request.getParameter("id"));

		BacklogEntry b = backlogEntryRepository.get(backlogEntryId);

		String name = request.getParameter("name");
		String description = request.getParameter("description");

		String priorityStr = request.getParameter("priority");
		String estimationStr = request.getParameter("estimation");

		if(name.equals(""))
		{
			session.setAttribute("error", "Name is required");
			request.setAttribute("entry", b);
			request.getRequestDispatcher("../backlogEntryForm.jsp").forward(request, response);
		}

		else if(description.equals(""))
		{
			session.setAttribute("error", "Description is required");
			request.setAttribute("entry", b);
			request.getRequestDispatcher("../backlogEntryForm.jsp").forward(request, response);
		}


		else if(!estimationStr.matches("[0-9]+"))
		{
			session.setAttribute("error", "Estimation is required and it's a integer");
			request.setAttribute("entry", b);
			request.getRequestDispatcher("../backlogEntryForm.jsp").forward(request, response);
		}

		else if(!priorityStr.matches("[0-9]+"))
		{
			session.setAttribute("error", "Priority is required and it's a integer");
			request.setAttribute("entry", b);
			request.getRequestDispatcher("../backlogEntryForm.jsp").forward(request, response);
		}

		else{


			int estimation = Integer.valueOf(estimationStr);
			int priority = Integer.valueOf(priorityStr);


			b.setName(name);
			b.setDescription(description);
			b.setPriority(priority);
			b.setEstimation(estimation);

			backlogEntryRepository.update(b);

			response.sendRedirect("../AgencyServlet");
		}


	}

}

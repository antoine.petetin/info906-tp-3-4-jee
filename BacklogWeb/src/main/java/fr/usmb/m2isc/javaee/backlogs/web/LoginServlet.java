package fr.usmb.m2isc.javaee.backlogs.web;

import fr.usmb.m2isc.javaee.backlogs.ejb.EmployeeRepositoryInterface;
import fr.usmb.m2isc.javaee.backlogs.jpa.Employee;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * Servlet utilisee pour creer un colis.
 */
@WebServlet("/login")
public class LoginServlet extends HttpServlet{

	//private static final long serialVersionUID = 5236668439173484090L;
	@EJB
	private EmployeeRepositoryInterface employeeRepository;

    /**
     * @see HttpServlet#HttpServlet()
     */
	public LoginServlet() {
		super();
	}


	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.getRequestDispatcher("./login.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		HttpSession session = request.getSession(true);


		try {
			String email = request.getParameter("email");
			String password = request.getParameter("password");

			if(!email.matches("^[a-zA-Z0-9_!#$%&'*+/=?`{|}~^.-]+@[a-zA-Z0-9.-]+$"))
			{
				session.setAttribute("error", "Email is not valid");
				request.getRequestDispatcher("./login.jsp").forward(request, response);
			}

			else if(password.length() <6)
			{
				session.setAttribute("error", "Password too short");
				request.getRequestDispatcher("./login.jsp").forward(request, response);
			}
			else
			{

				Employee e = employeeRepository.login(email, password);

				if (e == null) {
					session.setAttribute("error", "No user found with this email and this password !");
					request.getRequestDispatcher("./login.jsp").forward(request, response);
				} else {
					session.setAttribute("message", "Hello "+ e.getFirstName() + " " + e.getLastName() );
					session.setAttribute("employeeId",e.getId());
					response.sendRedirect("AgencyServlet");
				}
			}

		}catch (Exception e) {
			session.setAttribute("error", "An error has occured !");
			response.sendRedirect("./login.jsp");
		}

	}

}

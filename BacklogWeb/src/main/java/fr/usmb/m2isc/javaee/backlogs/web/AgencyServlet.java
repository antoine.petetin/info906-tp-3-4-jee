package fr.usmb.m2isc.javaee.backlogs.web;

import fr.usmb.m2isc.javaee.backlogs.ejb.AgencyRepositoryInterface;
import fr.usmb.m2isc.javaee.backlogs.ejb.BacklogEntryRepositoryInterface;
import fr.usmb.m2isc.javaee.backlogs.jpa.Agency;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Servlet utilisee pour l'écran principal
 */
@WebServlet("/")
public class AgencyServlet extends HttpServlet{

	private static final long serialVersionUID = 5236668439173484090L;

	@EJB
	private AgencyRepositoryInterface agencyRepository;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public AgencyServlet() {
		super();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		HttpSession session = request.getSession(true);

		if(session.getAttribute("employeeId") == null) {
			response.sendRedirect("./login");
			return;
		}

		if(session.getAttribute("agencySelectedId") == null)
		{
			response.sendRedirect("./agencyselection");
			return;
		}

		Agency agency = agencyRepository.get((int)session.getAttribute("agencySelectedId"));


		request.setAttribute("agency", agency);
		request.getRequestDispatcher("./home.jsp").forward(request, response);

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	}

}

package fr.usmb.m2isc.javaee.backlogs.web;

import fr.usmb.m2isc.javaee.backlogs.ejb.AgencyRepository;
import fr.usmb.m2isc.javaee.backlogs.ejb.AgencyRepositoryInterface;
import fr.usmb.m2isc.javaee.backlogs.ejb.BacklogEntryRepositoryInterface;
import fr.usmb.m2isc.javaee.backlogs.jpa.Agency;
import fr.usmb.m2isc.javaee.backlogs.jpa.BacklogEntry;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * Servlet utilisee pour creer une entrée dans un backlog.
 */
@WebServlet("/backlogentry/create")
public class CreateBacklogEntryServlet extends HttpServlet{

	private static final long serialVersionUID = 5236668439173484090L;
	@EJB
	private BacklogEntryRepositoryInterface backlogEntryRepository;
	@EJB
	private AgencyRepositoryInterface agencyRepository;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public CreateBacklogEntryServlet() {
		super();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession(true);
		if (session.getAttribute("employeeId") == null) {
			response.sendRedirect("../login");
			return;
		} else {

			request.getRequestDispatcher("../backlogEntryForm.jsp").forward(request, response);
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		HttpSession session = request.getSession(true);

		String name = request.getParameter("name");
		String description = request.getParameter("description");
		String priorityStr = request.getParameter("priority");
		String estimationStr = request.getParameter("estimation");

		if(name.equals(""))
		{
			session.setAttribute("error", "Name is required");
			request.getRequestDispatcher("../backlogEntryForm.jsp").forward(request, response);
		}
		else if(description.equals(""))
		{
			session.setAttribute("error", "Description is required");
			request.getRequestDispatcher("../backlogEntryForm.jsp").forward(request, response);
		}
		else if(!estimationStr.matches("[0-9]+"))
		{
			session.setAttribute("error", "Estimation is required and it's a integer");
			request.getRequestDispatcher("../backlogEntryForm.jsp").forward(request, response);
		}
		else if(!priorityStr.matches("[0-9]+"))
		{
			session.setAttribute("error", "Priority is required and it's a integer");
			request.getRequestDispatcher("../backlogEntryForm.jsp").forward(request, response);
		}
		else
		{

			int estimation = Integer.valueOf(estimationStr);
			int priority = Integer.valueOf(priorityStr);



			BacklogEntry b = new BacklogEntry(name,priority,estimation,description);

			int agencyId = (int)session.getAttribute("agencySelectedId");

			Agency a = agencyRepository.get(agencyId);
			a.addBacklogEntry(b);

			agencyRepository.update(a);


			response.sendRedirect("../AgencyServlet");
		}





	}

}

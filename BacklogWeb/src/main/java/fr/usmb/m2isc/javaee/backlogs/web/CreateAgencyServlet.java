package fr.usmb.m2isc.javaee.backlogs.web;

import fr.usmb.m2isc.javaee.backlogs.ejb.AgencyRepositoryInterface;
import fr.usmb.m2isc.javaee.backlogs.ejb.EmployeeRepositoryInterface;
import fr.usmb.m2isc.javaee.backlogs.jpa.Agency;
import fr.usmb.m2isc.javaee.backlogs.jpa.BacklogEntry;
import fr.usmb.m2isc.javaee.backlogs.jpa.Employee;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebServlet("/agency/create")
public class CreateAgencyServlet extends HttpServlet {

    private static final long serialVersionUID = 5236668439173484090L;
    @EJB
    private AgencyRepositoryInterface agencyRepository;
    @EJB
    private EmployeeRepositoryInterface employeeRepository;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public CreateAgencyServlet() {
        super();
    }

    /**
     * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession(true);
        if(session.getAttribute("employeeId") == null)
        {
            response.sendRedirect("../login");
            return;
        }else {
            request.getRequestDispatcher("../agencyForm.jsp").forward(request, response);
        }

    }

    /**
     * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        HttpSession session = request.getSession(true);

        String name = request.getParameter("name");

        if(name.equals(""))
        {
            session.setAttribute("error", "Name is required");
            request.getRequestDispatcher("../agencyForm.jsp").forward(request, response);
        }
        else
        {

            int employeeId = (int)session.getAttribute("employeeId");
            System.out.println(employeeId);
            Employee e = employeeRepository.get(employeeId);
            Agency a = new Agency(name);
            e.addAgency(a);

            employeeRepository.update(e);


            response.sendRedirect("../AgencyServlet");
        }

    }




}

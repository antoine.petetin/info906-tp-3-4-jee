package fr.usmb.m2isc.javaee.backlogs.web;

import fr.usmb.m2isc.javaee.backlogs.ejb.EmployeeRepositoryInterface;
import fr.usmb.m2isc.javaee.backlogs.jpa.Employee;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * Servlet utilisee pour creer un colis.
 */
@WebServlet("/logout")
public class LogoutServlet extends HttpServlet{

	//private static final long serialVersionUID = 5236668439173484090L;
	@EJB
	private EmployeeRepositoryInterface employeeRepository;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public LogoutServlet() {
		super();
	}


	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession(true);
		session.invalidate();
		response.sendRedirect("./login");
		return; // <--- Here.
	}

}

package fr.usmb.m2isc.javaee.backlogs.web;

import fr.usmb.m2isc.javaee.backlogs.ejb.AgencyRepositoryInterface;
import fr.usmb.m2isc.javaee.backlogs.ejb.EmployeeRepository;
import fr.usmb.m2isc.javaee.backlogs.ejb.EmployeeRepositoryInterface;
import fr.usmb.m2isc.javaee.backlogs.jpa.Agency;
import fr.usmb.m2isc.javaee.backlogs.jpa.Employee;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


/**
     * Servlet utilisee pour la sélection de l'agence
     */
    @WebServlet("/agencyselection")
    public class AgencySelectionServlet extends HttpServlet {

    private static final long serialVersionUID = 5236668439173484090L;

    @EJB
    private AgencyRepositoryInterface agencyRepository;
    @EJB
    private EmployeeRepositoryInterface employeeRepository;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public AgencySelectionServlet() {
        super();
    }

    /**
     * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        HttpSession session = request.getSession(true);
        if(session.getAttribute("employeeId") == null)
        {
            response.sendRedirect("./login");
            return;
        }else {

            int employeeId = (int) session.getAttribute("employeeId");

            System.out.println(employeeId);
            Employee e = employeeRepository.get(employeeId);

            request.setAttribute("agencies", e.getAgencies());

            request.getRequestDispatcher("/agencySelection.jsp").forward(request, response);
        }
    }

    /**
     * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        HttpSession session = request.getSession(true);

        int agencyId = Integer.valueOf(request.getParameter("agencyId"));

        session.setAttribute("agencySelectedId",agencyId);



        response.sendRedirect("AgencyServlet");

    }

}

package fr.usmb.m2isc.javaee.backlogs.web;

import fr.usmb.m2isc.javaee.backlogs.ejb.AgencyRepository;
import fr.usmb.m2isc.javaee.backlogs.ejb.AgencyRepositoryInterface;
import fr.usmb.m2isc.javaee.backlogs.ejb.BacklogEntryRepositoryInterface;
import fr.usmb.m2isc.javaee.backlogs.jpa.Agency;
import fr.usmb.m2isc.javaee.backlogs.jpa.BacklogEntry;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * Servlet utilisee pour voir une entrée en détails avec les commentaires.
 */
@WebServlet("/backlogentry/delete")
public class DeleteBacklogEntryServlet extends HttpServlet {

    private static final long serialVersionUID = 5236668439173484090L;
    @EJB
    private BacklogEntryRepositoryInterface backlogEntryRepository;
    @EJB
    private AgencyRepositoryInterface agencyRepository;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public DeleteBacklogEntryServlet() {
        super();
    }

    /**
     * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        HttpSession session = request.getSession(true);

        int agencyId = (int)session.getAttribute("agencySelectedId");

        Agency a = agencyRepository.get(agencyId);

        int backlogEntryId =  Integer.valueOf(request.getParameter("id"));

        BacklogEntry b = backlogEntryRepository.get(backlogEntryId);

        //Remove backlog with comments
        a.removeBacklogEntry(b);

        agencyRepository.update(a);

        backlogEntryRepository.remove(b);

        response.sendRedirect("../AgencyServlet");
    }

}

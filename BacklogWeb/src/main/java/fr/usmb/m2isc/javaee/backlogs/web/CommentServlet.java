package fr.usmb.m2isc.javaee.backlogs.web;

import fr.usmb.m2isc.javaee.backlogs.ejb.BacklogEntryRepositoryInterface;
import fr.usmb.m2isc.javaee.backlogs.ejb.CommentRepositoryInterface;
import fr.usmb.m2isc.javaee.backlogs.ejb.EmployeeRepositoryInterface;
import fr.usmb.m2isc.javaee.backlogs.jpa.BacklogEntry;
import fr.usmb.m2isc.javaee.backlogs.jpa.Comment;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Servlet utilisee pour voir une entrée en détails avec les commentaires.
 */
@WebServlet("/comment")
public class CommentServlet extends HttpServlet{

	@EJB
	private CommentRepositoryInterface commentRepository;

	@EJB
	private BacklogEntryRepositoryInterface backlogEntryRepository;

	@EJB
	private EmployeeRepositoryInterface employeeRepository;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public CommentServlet() {
		super();
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Comment comment = null;
		try{
			String commentDescription = request.getParameter("comment");
			int backlogId = Integer.parseInt(request.getParameter("backlogId"));
			if(commentDescription != null && !commentDescription.trim().isEmpty() && backlogId>0) {
				comment = new Comment(commentDescription);
				comment.setEmployee(employeeRepository.get(Integer.valueOf(request.getParameter("employeeId"))));
				comment.setBacklogEntry(backlogEntryRepository.get(backlogId));

				BacklogEntry backlogEntry = backlogEntryRepository.get(backlogId);
				backlogEntry.addComment(comment);

				backlogEntryRepository.update(backlogEntry);

			}

		}catch (Exception e){

		}finally {
			System.out.println(comment.toString());
			response.setContentType("application/json");
			response.setCharacterEncoding("UTF-8");
			response.getWriter().write(comment.toString());
		}
		//}
	}

}

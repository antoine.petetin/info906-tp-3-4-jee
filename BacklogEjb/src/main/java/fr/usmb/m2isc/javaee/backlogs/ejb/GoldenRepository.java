package fr.usmb.m2isc.javaee.backlogs.ejb;

import fr.usmb.m2isc.javaee.backlogs.jpa.Agency;
import fr.usmb.m2isc.javaee.backlogs.jpa.BacklogEntry;
import fr.usmb.m2isc.javaee.backlogs.jpa.Employee;

import javax.annotation.PostConstruct;
import javax.ejb.*;

@Startup
@Singleton
@Stateless
@Remote
public class GoldenRepository {

	@EJB
	private AgencyRepositoryInterface agencyRepository;
	@EJB
	private BacklogEntryRepositoryInterface backlogEntryRepository;
	@EJB
	private EmployeeRepositoryInterface employeeRepository;

	public GoldenRepository() {
	}

	@PostConstruct
	private void init(){

		Employee e1 = new Employee("Antoine", "Petetin","antoine.petetin@gmail.com", "123456");
		Employee e2 = new Employee("Stephane", "Talbot","stephane.talbot@gmail.com", "123456");

		Agency agency1 = new Agency("Easy Connect");


		BacklogEntry b1 = new BacklogEntry("En tant qu'admin, je peux gérer les utilisateurs",1,5,"ma description1");
		BacklogEntry b2 = new BacklogEntry("Etant tant qu'utilisateur, je peux me connecter", 3,10,"ma description2");

		backlogEntryRepository.createBacklogEntry(b1);
		backlogEntryRepository.createBacklogEntry(b2);
		//TODO use backlogEntryRepository.create

		agency1.addBacklogEntry(backlogEntryRepository.getAll().get(0));
		agency1.addBacklogEntry(backlogEntryRepository.getAll().get(1));

		agencyRepository.create(agency1);

		e1.addAgency(agencyRepository.getAll().get(0));
		e2.addAgency(agencyRepository.getAll().get(0));

		employeeRepository.create(e1);
		employeeRepository.create(e2);

		Employee e3 = new Employee("Anthony", "Bruhl","anthony.bruhl@gmail.com", "789123");

		Agency agency2 = new Agency("LeBonCoin");

		BacklogEntry b3 = new BacklogEntry("En tant que propriétaire, je peux ajouter un bien", 5,8,"ma description3");

		backlogEntryRepository.createBacklogEntry(b3);
		//TODO use backlogEntryRepository.create

		agency2.addBacklogEntry(backlogEntryRepository.getAll().get(2));

		agencyRepository.create(agency2);

		e3.addAgency(agencyRepository.getAll().get(1));

		employeeRepository.create(e3);

	}


}

package fr.usmb.m2isc.javaee.backlogs.ejb;

import fr.usmb.m2isc.javaee.backlogs.jpa.Agency;

import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Order;
import java.util.List;

@Stateless
@Remote
public class AgencyRepository extends Repository<Agency> implements AgencyRepositoryInterface {

	@PersistenceContext
	private EntityManager em;

	public AgencyRepository() {
		super(Agency.class);
	}

	//Usefull we calling super class methods !
	@Override
	protected EntityManager getEntityManager() {
		return em;
	}
}

package fr.usmb.m2isc.javaee.backlogs.jpa;


import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;


@Entity
public class BacklogEntry implements Serializable {

    @Id
    @GeneratedValue
    private int id;

    @NotNull
    private String name;

    @Column(updatable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date creationDate = Calendar.getInstance().getTime();

    @NotNull
    private int priority;

    @NotNull
    private int estimation;

    private String description;

    @OneToMany(fetch = FetchType.EAGER, cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REMOVE}, mappedBy = "backlogEntry")
    private List<Comment> comments = new ArrayList<>();

    @Transient
    private int nbComments = 0;

    public BacklogEntry() {
    }

    public BacklogEntry(String name, int priority, int estimation, String description) {
        this.name = name;
        this.priority = priority;
        this.estimation = estimation;
        this.description = description;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public int getPriority() {
        return priority;
    }

    public int getEstimation() {
        return estimation;
    }

    public String getDescription() {
        return description;
    }

    public List<Comment> getComments() {
        return comments;
    }

    public int getNbComments() {
        return comments.size();
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPriority(int priority) {
        this.priority = priority;
    }

    public void setEstimation(int estimation) {
        this.estimation = estimation;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public boolean equals(Object obj) {
        if(obj instanceof BacklogEntry)
        {
            return this.id == ((BacklogEntry) obj).id;
        }
        return false;
    }

    public void addComment(Comment comment) {
        comments.add(comment);
        comment.setBacklogEntry(this);
    }
}

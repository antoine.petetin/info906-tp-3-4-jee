package fr.usmb.m2isc.javaee.backlogs.ejb;

import fr.usmb.m2isc.javaee.backlogs.jpa.Comment;

public interface CommentRepositoryInterface extends RepositoryInterface<Comment>{
    Comment getByAttributes(int author, String description);
}
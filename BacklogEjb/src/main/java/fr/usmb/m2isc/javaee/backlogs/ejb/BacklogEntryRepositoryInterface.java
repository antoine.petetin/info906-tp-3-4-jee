package fr.usmb.m2isc.javaee.backlogs.ejb;

import fr.usmb.m2isc.javaee.backlogs.jpa.BacklogEntry;
import fr.usmb.m2isc.javaee.backlogs.jpa.Comment;

public interface BacklogEntryRepositoryInterface extends RepositoryInterface<BacklogEntry>{

    int createBacklogEntry(/*String name, int priority, int estimation, String description*/ BacklogEntry backlogEntry);

}
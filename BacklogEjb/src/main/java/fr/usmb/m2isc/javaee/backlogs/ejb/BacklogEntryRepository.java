package fr.usmb.m2isc.javaee.backlogs.ejb;

import fr.usmb.m2isc.javaee.backlogs.jpa.BacklogEntry;
import fr.usmb.m2isc.javaee.backlogs.jpa.Comment;

import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Stateless
@Remote
public class BacklogEntryRepository extends Repository<BacklogEntry> implements BacklogEntryRepositoryInterface {
	
	@PersistenceContext
	private EntityManager em;

	@EJB
	private AgencyRepositoryInterface placeRepository;

	public BacklogEntryRepository() {
		super(BacklogEntry.class);
	}

	@Override
	protected EntityManager getEntityManager() {
		return em;
	}

	@Override
	public int createBacklogEntry(/*String name, int priority, int estimation, String description*/ BacklogEntry backlogEntry) {
		//BacklogEntry backlogEntry = new BacklogEntry(name, priority, estimation, description);
		em.persist(backlogEntry);
		return backlogEntry.getId();
	}


}

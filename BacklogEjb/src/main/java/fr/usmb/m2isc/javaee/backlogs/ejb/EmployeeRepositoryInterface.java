package fr.usmb.m2isc.javaee.backlogs.ejb;

import fr.usmb.m2isc.javaee.backlogs.jpa.Agency;
import fr.usmb.m2isc.javaee.backlogs.jpa.Employee;

import java.util.List;

public interface EmployeeRepositoryInterface extends RepositoryInterface<Employee>{

    Employee login(String email, String password);

    List<Agency> getEmployeeAgencies(Employee e);

}
package fr.usmb.m2isc.javaee.backlogs.jpa;


import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;


@Entity
public class Agency implements Serializable {

    @Id
    @GeneratedValue
    private int id;

    @NotNull
    private String name;


    @OneToMany(fetch = FetchType.EAGER)
    @OrderBy("priority desc")
    private List<BacklogEntry> backlog = new ArrayList<>();

    public Agency() {
    }

    public Agency(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public List<BacklogEntry> getBacklog() {
        return backlog;
    }

    public void addBacklogEntry(BacklogEntry entry){
        backlog.add(entry);
    }

    public void removeBacklogEntry(BacklogEntry entry){
        backlog.remove(entry);
    }



}

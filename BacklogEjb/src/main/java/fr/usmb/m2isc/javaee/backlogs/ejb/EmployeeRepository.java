package fr.usmb.m2isc.javaee.backlogs.ejb;

import fr.usmb.m2isc.javaee.backlogs.jpa.Agency;
import fr.usmb.m2isc.javaee.backlogs.jpa.Employee;

import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import java.util.List;

@Stateless
@Remote
public class EmployeeRepository extends Repository<Employee> implements EmployeeRepositoryInterface {

	@PersistenceContext
	private EntityManager em;

	public EmployeeRepository() {
		super(Employee.class);
	}

	//Usefull we calling super class methods !
	@Override
	protected EntityManager getEntityManager() {
		return em;
	}

	public List<Agency> getEmployeeAgencies(Employee e){

		return em.merge(e).getAgencies();

	}

	@Override
	public Employee login(String email, String password) {

		try {

			TypedQuery<Employee> query = em.createQuery("select e from Employee e where e.email = :email and e.password = :password", Employee.class);
			query.setParameter("email", email);
			query.setParameter("password",password);
			return query.getSingleResult();

		}catch (NoResultException e)
		{
			return null;
		}

	}
}

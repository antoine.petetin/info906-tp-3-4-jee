package fr.usmb.m2isc.javaee.backlogs.jpa;


import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;


@Entity
public class Comment implements Serializable {

    @Id
    @GeneratedValue
    private int id;

    @NotNull
    private String content;

    @Column(name = "creation_date", columnDefinition="TIMESTAMP DEFAULT CURRENT_TIMESTAMP", insertable = false, updatable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date creationDate = Calendar.getInstance().getTime();

    @ManyToOne(fetch = FetchType.EAGER)
    private Employee employee = new Employee();

    @ManyToOne
    private BacklogEntry backlogEntry = new BacklogEntry();

    public Comment() {
    }

    public Comment(String content){
        this.content = content;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public String getFormattedDate(){
        if(creationDate != null) {
            DateFormat formatter = DateFormat.getDateTimeInstance(
                    DateFormat.MEDIUM,
                    DateFormat.MEDIUM, new Locale("EN", "en"));
            return formatter.format(creationDate);
        }else
            return null;
    }

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    public BacklogEntry getBacklogEntry() {
        return backlogEntry;
    }

    public void setBacklogEntry(BacklogEntry backlogEntry) {
        this.backlogEntry = backlogEntry;
    }

    @Override
    public String toString() {
        if(employee != null){

            return "{" +
                    "\"id\": "+id + "," +
                    "\"content\": \"" + content + "\"," +
                    "\"creationDate\": \"" + getFormattedDate() + "\"," +
                    "\"employee\": \"" + employee.getFullName()+"\"}";
        }else
            return "{" +
                    "\"id\": "+id + "," +
                    "\"creationDate\": \"" + creationDate.toString() + "\"," +
                    "\"content\": \"" + content + "\"}";
    }
}

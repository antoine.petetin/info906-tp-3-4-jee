package fr.usmb.m2isc.javaee.backlogs.ejb;

import fr.usmb.m2isc.javaee.backlogs.jpa.Agency;

public interface AgencyRepositoryInterface extends RepositoryInterface<Agency>{

}
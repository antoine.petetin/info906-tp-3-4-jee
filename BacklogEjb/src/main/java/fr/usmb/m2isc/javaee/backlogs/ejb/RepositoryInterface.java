package fr.usmb.m2isc.javaee.backlogs.ejb;

import java.util.List;

public interface RepositoryInterface<T> {
    void create(T entity);
    void update(T entity);
    void remove(T entity);
    T get(int id);
    List<T> getAll();
    int count();
}
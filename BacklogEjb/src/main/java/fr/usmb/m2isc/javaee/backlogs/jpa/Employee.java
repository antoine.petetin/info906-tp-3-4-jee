package fr.usmb.m2isc.javaee.backlogs.jpa;


import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;


@Entity
public class Employee implements Serializable {

    @Id
    @GeneratedValue
    private int id;

    @NotNull
    @Column(name = "firstname")
    private String firstName;

    @NotNull
    @Column(name = "lastname")
    private String lastName;

    @NotNull
    private String email;

    @NotNull
    private String password;

    @ManyToMany(fetch = FetchType.EAGER)
    private List<Agency> agencies = new ArrayList<>();

    @OneToMany(mappedBy = "employee")
    private List<Comment> comments;

    public Employee() {
    }

    public Employee(String firstName, String lastName, String email, String password) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.password = password;
    }

    public int getId() {
        return id;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getFullName(){
        if(firstName != null && lastName != null)
            return firstName + " " + lastName;
        else
            return null;
    }

    public String getEmail() {
        return email;
    }

    public String getPassword() {
        return password;
    }

    public List<Agency> getAgencies() {
        return agencies;
    }

    public void addAgency(Agency agency){
            agencies.add(agency);
    }
}

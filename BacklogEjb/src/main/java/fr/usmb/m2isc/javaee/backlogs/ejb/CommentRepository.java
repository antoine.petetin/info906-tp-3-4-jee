package fr.usmb.m2isc.javaee.backlogs.ejb;

import fr.usmb.m2isc.javaee.backlogs.jpa.Comment;

import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaQuery;
import java.util.Date;

@Stateless
@Remote
public class CommentRepository extends Repository<Comment> implements CommentRepositoryInterface {

	@PersistenceContext
	private EntityManager em;

	public CommentRepository() {
		super(Comment.class);
	}

	//Usefull we calling super class methods !
	@Override
	protected EntityManager getEntityManager() {
		return em;
	}

	@Override
	public Comment getByAttributes(int author, String description) {
		return em.createQuery("select c from Comment c where c.content = :content and c.employee.id = :id",Comment.class)
				.setParameter("content",description)
				.setParameter("id",author)
				.getSingleResult();
	}
}

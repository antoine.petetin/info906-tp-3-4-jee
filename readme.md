# TP2 Gestion de backlog et d'agences JavaEE 7

Pour faire simple, les agences vont chacunes avoir un backlog. Un backlog est une liste d'entrée avec différents attributs (nom, priorité, estimation, commentaires ...).
Les employés vont se connecter sur l'application et pouvoir gérer le backlog d'une agence dont ils font partie.
Il vont pouvoir aussi créer une agence. 

## How to run

* Mettre la base de données du serveur d'application en route (Glassfish)
`./asadmin start-database` dans le dossier glassfish5/bin/
* Déployer BacklogWebEar (avec Intellij par exemple) puis taper l'url [http://localhost:8080/BacklogWeb ](http://localhost:8080/BacklogWeb ) dans votre navigateur
* Liste des différents utilisateurs avec leurs agences :
    * Agence "Easy Connect"
        * stephane.talbot@gmail.com (mot de passe : 123456)
        * antoine.petetin@gmail.com (mot de passe : 123456)
    * Agence "LeBonCoin"
        * anthony.bruhl@gmail.com (mot de passe : 789123)

Nous utilisons Bootstrap et jQuery avec des liens publiques ! Vous devez être connecté à Internet !
 
## Serveur d'application
Glassfish 5.0.1

## Classe persistante
* Agency
    * id
    * le nom de l'agence
    * le backlog (liste de BacklogEntry)

* Employee 
    * id
    * le prénom
    * le nom
    * l'email
    * le mot de passe
    * la liste d'agences auquel il est associé (relation bidirectionnelle)
    * les commentaires qu'il a créé (relation bidirectionnelle)

* BacklogEntry 
    * id
    * le nom de l'entrée
    * sa date de création
    * sa priorité
    * son estimation
    * sa description
    * la liste des commentaires 

* Comment
    * id
    * le texte
    * la date de création
    * l'employée qui l'a créé (relation bidirectionnelle)

## EJB
Les ejb sont conçus pour être déployés en remote grâce à l'annotation `@Remote` sur les EJB. Les entités JPA doivent être sérialisable.

* GoldenRepository : Enregistre des données dans la base de données
* Repository : Classe abstraite contenant le CRUD
* RepositoryInterface : Interface du CRUD
* AgencyRepository : DAO de la classe Agency
* AgencyRepositoryInterface : Interface du DAO de la classe Agency
* BacklogEntryRepository : DAO de la classe Backlog
* BacklogEntryRepositoryInterface : Interface du DAO de la classe BacklogEntry
* CommentRepository : DAO de la classe Comment
* CommentRepositoryInterface : Interface du DAO de la classe Comment
* EmployeeRepository : DAO de la classe Employee
* EmployeeRepositoryInterface : Interface du DAO de la classe Employee

## Servlets

* LoginServlet : Permet à l'employé de s'identifier (id stocké dans la session)
* LogoutServlet : Permet à l'employé de se déconnecter (invalidation de la session)
* AgencySelectionServlet : Permet à l'employé qui vient de se connecter de choisir un agence (afin de gérer le backlog)
* AgencyServlet : Permet à l'employé de gérer le backlog d'une agence
* CreateBacklogEntryServlet : Permet à l'employé de créer une entrée dans le backlog
* UpdateBacklogEntryServlet : Permet à l'employé de modifier une entrée existante dans le backlog
* DeleteBacklogEntryServlet : Permet à l'employé de supprimer une entrée existante dans le backlog
* CommentServlet : Permet à l'employé d'ajouter un commentaire sur une entrée du backlog (méthode `doPost` appelée en Ajax)
* CreateAgencyServlet : Permet à l'employé de créer une nouvelle agence

## Les vues JSP
Nous avons mis en place un template générique. Chaque vue sera chargée dans ce template. Ce template peut se trouver dans le dossier `webapp/WEB-INF/tags/layout.tag`. Il sera appelé à chaque fois qu'on charge une page.

Nous utilisons Bootstrap et jQuery avec des liens publiques ! Vous devez être connecté à Internet !

* login.jsp: formulaire de connexion de l'employé
* agencySelection.jsp: choix de l'agence
* home.jsp : vue principale qui affiche le backlog
* agencyForm: formulaire de création d'une agence
* backlogEntryForm: formulaire de création et de modification d'une entrée du backlog
